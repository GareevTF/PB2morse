/************************************************* Library ******************************************************************/
#include "stm32f10x.h"


/************************************************* Defines *******************************************************************/
#define LED_MORSE_ON	GPIOB->BSRR =GPIO_BSRR_BS6
#define	LED_MORSE_OFF	GPIOB->BSRR =GPIO_BSRR_BR6
#define LED3_ON				GPIOB->BSRR =GPIO_BSRR_BS5;
#define LED3_OFF			GPIOB->BSRR =GPIO_BSRR_BR5;


/************************************************* Use function ******************************************************************/
void InitGPIO (void);
void InitSysTick (void);
