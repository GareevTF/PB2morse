/************************************************* Library ******************************************************************/
#include "stm32f10x.h"
#include "delay.h"
#include "sysInit.h"
#include "stddef.h"

/************************************************* Use function *************************************************************/

char *Symbol2MorseSignals( char symbol);
void SendSignals(char*signals);
void SendSignal(char signal);
void SendMorse(char *message);
