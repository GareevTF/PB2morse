/************************************************* Library ******************************************************************/
#include "sysInit.h"


/************************************************* Use function ******************************************************************/

void InitGPIO (void)
	{
	// ���������� ������������ ����� B � APB2
	RCC->APB2ENR 	|= RCC_APB2ENR_IOPBEN;
		
	// ������������� CRL ��������. 
	GPIOB->CRL	&= ~(GPIO_CRL_CNF5	|	GPIO_CRL_CNF6);		// ���������� ���� CNF ��� ���� 5 � 6 ����� B. ����� 00 - Push-Pull 
	GPIOB->CRL 	|= GPIO_CRL_MODE5_0	|	GPIO_CRL_MODE6_0;	// ���������� ��� MODE0 ��� ������ ����. ����� MODE01 = Max Speed 10MHz

	}
	
//��������� SysTick �� ��������� ���������� ��� � ��	
void InitSysTick (void)
	{
	if (SysTick_Config(SystemCoreClock / 1000))
					{   
					LED3_ON;		//  LED3 ��������� ����� - ������������� ������ ��������� SysTick 
					while(1);
					}
	}
	
