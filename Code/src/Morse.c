/************************************************* Library ******************************************************************/
#include "Morse.h"

/************************************************* Defines ******************************************************************/
#define	DOT_DELAY			250
#define	DASH_DELAY		3*DOT_DELAY
#define	SYMBOLS_DELAY 2*DOT_DELAY
#define WORD_DELAY		6*DOT_DELAY

#define NumOfSymbols 8
char *alphabet[NumOfSymbols][2]=	
														{
															{"H","...."},
															{"E","."},
															{"L",".-.."},
															{"O","---"},
															{"W",".--"},
															{"R",".-."},
															{"D","-.."},
															{" ","_"}
														};

														
/************************************************* Use function *************************************************************/

void SendMorse(char *message)
{	if(message)
	while(*message)
			{
				SendSignals(Symbol2MorseSignals(*message));
				Delay(SYMBOLS_DELAY);
				message++;
			}	
}

char *Symbol2MorseSignals( char symbol)
		{		
			for(uint8_t i=0;i<NumOfSymbols;i++)
			if (symbol==*alphabet[i][0]) 				
			return alphabet[i][1];															
			
			return NULL;
		}
		

void SendSignals(char*signals)
		{	if(signals)
			while(*signals)
				{	SendSignal(*signals);
					Delay(DOT_DELAY);
					signals++;
				}	
		}
	
void SendSignal(char signal)
{
	if (signal=='.')	{
											LED_MORSE_ON;
											Delay(DOT_DELAY);
											LED_MORSE_OFF;
										}
	if (signal=='-')	{
											LED_MORSE_ON;
											Delay(DASH_DELAY);
											LED_MORSE_OFF;
										}	
	if (signal=='_')	{
											Delay(WORD_DELAY);
										}	
}

